import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;

public class ApiTest_InvalidPeselTest {

    @Test
    public void givenPeselTooLong_whenSendingValidationRequest_thenResponseContainsPeselTooLong() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=8807010330201");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"8807010330201\",\"isValid\":false,\"birthDate\":null,\"sex\":null,\"errors\":[{\"errorCode\":\"INVL\",\"errorMessage\":\"Invalid length. Pesel should have exactly 11 digits.\"}]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselTooLong_whenSendingValidationRequest_bdd() {
        given()
                .when()
                .get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=843986724476545948568")
                .then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void givenPeselTooShort_whenSendingValidationRequest_thenResponseContainsPeselTooShort() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=880701033");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"880701033\",\"isValid\":false,\"birthDate\":null,\"sex\":null,\"errors\":[{\"errorCode\":\"INVL\",\"errorMessage\":\"Invalid length. Pesel should have exactly 11 digits.\"}]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselTooShort_whenSendingValidationRequest_bdd() {
        given()
                .when()
                .get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=18923834")
                .then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void givenPeselIncludesInvalidCharacters_whenSendingValidationRequest_thenResponseContainsPeselWithInvalidCharacters() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=8801072574@!");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"8801072574@!\",\"isValid\":false,\"birthDate\":null,\"sex\":null,\"errors\":[{\"errorCode\":\"NBRQ\",\"errorMessage\":\"Invalid characters. Pesel should be a number.\"}]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselIncludesInvalidCharacters_whenSendingValidationRequest_bdd() {
        given()
                .when()
                .get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=091234%!@#$&")
                .then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void givenPeselIncludesInvalidYear_whenSendingValidationRequest_thenResponseContainsPeselInvalidYear() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=19130725740");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"19130725740\",\"isValid\":false,\"birthDate\":null,\"sex\":\"Female\",\"errors\":[{\"errorCode\":\"INVC\",\"errorMessage\":\"Check sum is invalid. Check last digit.\"},{\"errorCode\":\"INVY\",\"errorMessage\":\"Invalid year.\"},{\"errorCode\":\"INVM\",\"errorMessage\":\"Invalid month.\"}]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselIncludesInvalidYearForFemaleBornBefore1800_whenSendingValidationRequest_thenResponseContainsPeselInvalidYear() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=00790725740");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"00790725740\",\"isValid\":false,\"birthDate\":null,\"sex\":\"Female\",\"errors\":[{\"errorCode\":\"INVC\",\"errorMessage\":\"Check sum is invalid. Check last digit.\"},{\"errorCode\":\"INVY\",\"errorMessage\":\"Invalid year.\"},{\"errorCode\":\"INVM\",\"errorMessage\":\"Invalid month.\"}]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselIncludesInvalidYear_whenSendingValidationRequest_bdd() {
        given()
                .when()
                .get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=00130725740")
                .then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void givenPeselIncludesInvalidMonth_whenSendingValidationRequest_thenResponseContainsPeselInvalidMonth() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=88130725740");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"88130725740\",\"isValid\":false,\"birthDate\":null,\"sex\":\"Female\",\"errors\":[{\"errorCode\":\"INVC\",\"errorMessage\":\"Check sum is invalid. Check last digit.\"},{\"errorCode\":\"INVY\",\"errorMessage\":\"Invalid year.\"},{\"errorCode\":\"INVM\",\"errorMessage\":\"Invalid month.\"}]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselIncludesInvalidDay_whenSendingValidationRequest_thenResponseContainsPeselInvalidDay() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=88073303302");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"88073303302\",\"isValid\":false,\"birthDate\":null,\"sex\":\"Female\",\"errors\":[{\"errorCode\":\"INVC\",\"errorMessage\":\"Check sum is invalid. Check last digit.\"},{\"errorCode\":\"INVD\",\"errorMessage\":\"Invalid day.\"}]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselForFemaleIncludesInvalidCheckSum_whenSendingValidationRequest_thenResponseContainsPeselInvalidCheckSum() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=89631681384");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"89631681384\",\"isValid\":false,\"birthDate\":\"2289-03-16T00:00:00\",\"sex\":\"Female\",\"errors\":[{\"errorCode\":\"INVC\",\"errorMessage\":\"Check sum is invalid. Check last digit.\"}]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselForMaleIncludesInvalidCheckSum_whenSendingValidationRequest_thenResponseContainsPeselInvalidCheckSum() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=70060708178");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"70060708178\",\"isValid\":false,\"birthDate\":\"1970-06-07T00:00:00\",\"sex\":\"Male\",\"errors\":[{\"errorCode\":\"INVC\",\"errorMessage\":\"Check sum is invalid. Check last digit.\"}]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselForFemaleIncludesInvalidLastDigit_whenSendingValidationRequest_thenResponseContainsPeselInvalidLastDigit() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=88070103308");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"88070103308\",\"isValid\":false,\"birthDate\":\"1988-07-01T00:00:00\",\"sex\":\"Female\",\"errors\":[{\"errorCode\":\"INVC\",\"errorMessage\":\"Check sum is invalid. Check last digit.\"}]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselForMaleIncludesInvalidLastDigit_whenSendingValidationRequest_thenResponseContainsPeselInvalidLastDigit_bdd() {
        given()
                .when()
                .get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=02070803627")
                .then()
                .assertThat()
                .statusCode(200);
    }
}