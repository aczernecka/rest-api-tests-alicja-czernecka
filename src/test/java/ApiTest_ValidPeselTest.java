import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;

public class ApiTest_ValidPeselTest {

    @Test
    public void givenPeselIsValidForFemale_whenSendingValidationRequest_thenResponseIsCorrect() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=88010725740");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"88010725740\",\"isValid\":true,\"birthDate\":\"1988-01-07T00:00:00\",\"sex\":\"Female\",\"errors\":[]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselIsValidForMale_whenSendingValidationRequest_thenResponseIsCorrect() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=88010732191");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"88010732191\",\"isValid\":true,\"birthDate\":\"1988-01-07T00:00:00\",\"sex\":\"Male\",\"errors\":[]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselIsValidForMaleBornIn1901_whenSendingValidationRequest_thenResponseIsCorrect() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=01062478237");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"01062478237\",\"isValid\":true,\"birthDate\":\"1901-06-24T00:00:00\",\"sex\":\"Male\",\"errors\":[]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselIsValidForFemaleBornIn1901_whenSendingValidationRequest_thenResponseIsCorrect() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=00042435189");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"00042435189\",\"isValid\":true,\"birthDate\":\"1900-04-24T00:00:00\",\"sex\":\"Female\",\"errors\":[]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselIsValidForMaleBornIn1999_whenSendingValidationRequest_thenResponseIsCorrect() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=99090477993");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"99090477993\",\"isValid\":true,\"birthDate\":\"1999-09-04T00:00:00\",\"sex\":\"Male\",\"errors\":[]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselIsValidForFemaleBornIn1999_whenSendingValidationRequest_thenResponseIsCorrect() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=99101343224");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"99101343224\",\"isValid\":true,\"birthDate\":\"1999-10-13T00:00:00\",\"sex\":\"Female\",\"errors\":[]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselIsValidForMaleBornIn2000_whenSendingValidationRequest_thenResponseIsCorrect() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=00272481455");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"00272481455\",\"isValid\":true,\"birthDate\":\"2000-07-24T00:00:00\",\"sex\":\"Male\",\"errors\":[]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselIsValidForFemaleBornIn2000_whenSendingValidationRequest_thenResponseIsCorrect() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=00323021647");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"00323021647\",\"isValid\":true,\"birthDate\":\"2000-12-30T00:00:00\",\"sex\":\"Female\",\"errors\":[]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselIsValidForMaleBornIn2088_whenSendingValidationRequest_thenResponseIsCorrect() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=88251727512");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"88251727512\",\"isValid\":true,\"birthDate\":\"2088-05-17T00:00:00\",\"sex\":\"Male\",\"errors\":[]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselIsValidForFemaleBornIn2099_whenSendingValidationRequest_thenResponseIsCorrect() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=99231685386");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"99231685386\",\"isValid\":true,\"birthDate\":\"2099-03-16T00:00:00\",\"sex\":\"Female\",\"errors\":[]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselIsValidForMaleBornIn2100_whenSendingValidationRequest_thenResponseIsCorrect() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=00482834939");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"00482834939\",\"isValid\":true,\"birthDate\":\"2100-08-28T00:00:00\",\"sex\":\"Male\",\"errors\":[]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselIsValidForFemaleBornIn2100_whenSendingValidationRequest_thenResponseIsCorrect() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=00422823526");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"00422823526\",\"isValid\":true,\"birthDate\":\"2100-02-28T00:00:00\",\"sex\":\"Female\",\"errors\":[]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselIsValidForMaleBornIn2200_whenSendingValidationRequest_thenResponseIsCorrect() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=00690376913");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"00690376913\",\"isValid\":true,\"birthDate\":\"2200-09-03T00:00:00\",\"sex\":\"Male\",\"errors\":[]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselIsValidForFemaleBornIn2200_whenSendingValidationRequest_thenResponseIsCorrect() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=00712076809");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"00712076809\",\"isValid\":true,\"birthDate\":\"2200-11-20T00:00:00\",\"sex\":\"Female\",\"errors\":[]}";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenPeselIsValidForFemaleBornIn2200_ResponseIsCorrect_bdd() {
        given()
                .when()
                .get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=00312203425")
                .then()
                .assertThat()
                .statusCode(200);
    }
}
