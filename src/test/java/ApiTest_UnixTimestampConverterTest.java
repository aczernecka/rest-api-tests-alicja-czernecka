import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class ApiTest_UnixTimestampConverterTest {

    @Test
    public void givenUnixTimestamp_whenSendingConvertingRequest_thenResponseContainsUTCDateTime() {
        Response response = get("https://showcase.api.linx.twenty57.net/UnixTime/fromunix?timestamp=1549892280");
        String actualBody = response.getBody().asString();
        String expectedBody = "\"2019-02-11 13:38:00\"";
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenUnixTimestamp_whenSendingConvertingRequest_thenResponseContainsErrorConflict() {
        Response response = get("https://showcase.api.linx.twenty57.net/UnixTime/fromunixtimestamp?unixtimestamp=-1385760139247");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"Error\":\"fromunixtimestamp.FromUnixTimestamp: Value to add was out of range.\\r\\nParameter name: value\"}";
        Assert.assertEquals(response.statusCode(), 409);
        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void givenUnixTimestampIsEmpty_whenSendingConvertingRequest_thenResponseStatusCodeBadRequest() {
        Response response = get("https://showcase.api.linx.twenty57.net/UnixTime/fromunix?timestamp=");
        Assert.assertEquals(response.statusCode(), 400);
    }
}