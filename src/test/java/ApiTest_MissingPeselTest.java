import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class ApiTest_MissingPeselTest {

    @Test
    public void givenPeselParameterIsEmpty_whenSendingValidationRequest_thenResponseStatusCodeBadRequest() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=");
        Assert.assertEquals(response.statusCode(), 400);
    }
}
